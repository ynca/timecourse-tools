function [ onsets ] = parseEV( FSL_EV_filename )
%parseEV - Reads an FSL EV file (3 column format) and returns vector of
%onsets (in seconds)

fid = fopen(FSL_EV_filename);

onset_vector = zeros(1,1000);

for i = 1:1000,
    tline = fgetl(fid);
    if ~ischar(tline), break, end
    tvector = sscanf(tline, '%f');
    onset_vector(i) = tvector(1);
end    

onsets = onset_vector(1:(i-1));

fclose(fid);

end


clear all;

myPath = fileparts(mfilename('fullpath')); %Finds directory with this script

EVfilename = [myPath '/other/TRoN_testEV.txt'];
TCdir = [myPath '/timecourses/'];
TCfilenames = dir([TCdir 'FMRI102_*']);

for i = 1:length(TCfilenames),
    timecourse{i} = parseTC([TCdir TCfilenames(i).name]);
    onsets{i} = parseEV(EVfilename);
end

settings.tr = 0.625;
settings.baselineType = 'run';
settings.baselineStart = -4;
settings.baselineEnd = 0;
settings.volsBefore = 10;
settings.volsAfter = 32;

EventMat = eventAvg(timecourse, onsets, settings);

%To be offloaded to separate function at some point...
tr_vector = (-settings.volsBefore:settings.volsAfter)*settings.tr;
EventMat_avg = nanmean(EventMat, 2);

plotTitle = 'Visual pathway activation from checkerboard (9 conds collapsed)';
gray = [0.85 0.85 0.85];

plotCI = 1;
if plotCI, %Plot 95 percent confidence intervals
    err = (nanstd(EventMat,0,2)/sqrt(size(EventMat,2)))*1.96;
    plot(tr_vector, EventMat_avg+err, 'LineWidth', 1, 'Color', gray);
    hold on;
    plot(tr_vector, EventMat_avg-err, 'LineWidth', 1, 'Color', gray);
    x = [tr_vector, fliplr(tr_vector)];
    inBetween = [(EventMat_avg-err); flipud(EventMat_avg+err)];
    fill(x, inBetween, gray);
end

plot(tr_vector, EventMat_avg, 'LineWidth', 2, 'Color', [0 0 0]);
xlim([min(tr_vector)-1 max(tr_vector)+1]);
xlabel('Time from event onset (s)', 'fontsize', 14);
ylabel('Percent signal change', 'fontsize', 14);
set(gca, 'fontsize', 14);
title(plotTitle,'fontsize',14);

hold off;
#!/bin/bash

#This script uses fslmeants with a mask file and a 4D fMRI file to extract the mean signal across all mask=1 locations. The output is a text file of these averages in MR units.

MASK=masks/FMRI102_zstat1_thr4.nii.gz #Binarized mask file

MNAME=(${MASK//// })
FSUFFIX=${MNAME[1]} #Suffix for timecourse files

for FILE in data/*.nii.gz
do
 echo Extracting TC from file $FILE
 FNAME=(${FILE//// })
 FNAMECOMP=(${FNAME[1]//_/ })
 #Extract timecourse and then normalize by average of mask values; construction of the filename is currently idiosyncratic.
 TCNAME=timecourses/${FNAMECOMP[0]}_${FNAMECOMP[1]}_$FSUFFIX.txt
 echo $TCNAME
 fslmeants -i $FILE -o $TCNAME -m $MASK
done

echo Script execution time: $SECONDS s
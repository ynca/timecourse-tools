function EventMat = eventAvg(timecourse, onsets, settings)

% Produces a matrix of event timecourse x number of events, with events
% converted to a percent signal change based on the specified baseline type
% and range.

% Inputs are:
%   timecourse - Vector or Cell array
%   onsets - Vector or Cell array
%   settings - Struct
%       .tr - Number in seconds
%       .baselineType - 'event' or 'run' (calculated for each onset or each run)
%       .baselineStart - -4 (in volumes relative to event onset)
%       .baselineEnd - 0
%       .volsBefore - 10 (volumes to be included in EventMat before event onset)
%       .volsAfter - 32 (volumes to be included in EventMat after event onset)

%Checks
if ~strcmp(settings.baselineType, 'event') && ~strcmp(settings.baselineType, 'run'),    
    error('Invalid baseline computation type.');
end

%Set timecourse and onsets to cell arrays if they are currently vectors
if ~iscell(timecourse),
    temp = timecourse; clear timecourse; timecourse{1} = temp;
end 
if ~iscell(onsets),
    temp = onsets; clear onsets; onsets{1} = temp;
end

%Convert onsets in seconds to TRs; find total number of events
numOfEvents = 0;
firstOnset = nan(1, length(onsets)); %Set up vectors for ProtoMatrix indices for each run
lastOnset = firstOnset;
for i = 1:length(onsets),
    onsets_TR{i} = round((onsets{i}/settings.tr)+1);
    firstOnset(i) = numOfEvents + 1;
    numOfEvents = numOfEvents + length(onsets_TR{i});
    lastOnset(i) = numOfEvents;
end

%Set up matrix for handling events
eventLength = 1 + settings.volsBefore + settings.volsAfter; %Size of each event in TRs (data points)
ProtoMatrix = nan(eventLength, numOfEvents);

%Fill protomatrix with raw MR timecourse numbers for each event
Counter = 1;
for RunIndex = 1:size(timecourse, 2), % Step through multiple runs
    for EventIndex = 1:length(onsets_TR{RunIndex}), % Step through each event
        Estart = onsets_TR{RunIndex}(EventIndex);
        Eend = onsets_TR{RunIndex}(EventIndex); % Events are currently defined as a particular TR
        if Estart - settings.volsBefore > 0, % Accomodate desired ranges exceeding the dataset dimensions
            if Eend + settings.volsAfter <= length(timecourse{RunIndex}),
                event = timecourse{RunIndex}((Estart-settings.volsBefore):(Eend+settings.volsAfter));
                ProtoMatrix(1:length(event), Counter) = event;
            else
                event = timecourse{RunIndex}((Estart-settings.volsBefore):length(timecourse{RunIndex}));
                ProtoMatrix(1:length(event), Counter) = event;
            end
        else
            temp = settings.volsBefore + 2 - Estart;
            if Eend + settings.volsAfter <= length(timecourse{RunIndex}),
                event = timecourse{RunIndex}(1:(Eend+settings.volsAfter));
                ProtoMatrix(temp:(length(event)+temp-1), Counter) = event;
            else
                event = timecourse{RunIndex}(1:length(timecourse{RunIndex}));
                ProtoMatrix(temp:(length(event)+temp-1), Counter) = event;
            end
        end
        Counter = Counter + 1;
    end
end

%Calculate baselines
BaselineVector = nanmean(ProtoMatrix((settings.baselineStart+settings.volsBefore+1):...
    (settings.baselineEnd+settings.volsBefore+1), :), 1);
if strcmp(settings.baselineType, 'run'), %For run-based baselines
    for i = 1:length(firstOnset),
        BaselineVector(firstOnset(i):lastOnset(i)) = ...
            nanmean(BaselineVector(firstOnset(i):lastOnset(i)));
    end
end

%Convert values into percent signal change
for i = 1:size(ProtoMatrix, 2),
    ProtoMatrix(:, i) = ((ProtoMatrix(:, i) - BaselineVector(i)) ...
        / BaselineVector(i)) * 100;
end

%Create final output variables
EventMat = ProtoMatrix;


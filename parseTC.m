function [ timecourse ] = parseTC( nifti_timecourse_filename )
%parseTC Creates vector from TC file in text format.

fid = fopen(nifti_timecourse_filename);
TC_cell = textscan(fid, '%f');
timecourse = TC_cell{1};

end

